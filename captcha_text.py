from google.cloud import vision
import io
import os
key = 'AIzaSyDbE9YL4nXp9O1U3AeD0LY5CEzAnUOoYd4'
dir_path = os.path.dirname(os.path.realpath(__file__))
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join(os.curdir, dir_path+'/rpaocr-283109-9fa5ddb77e44.json')

class CaptchaText:
    def detect_text(self, captcha_img_path):
        """Detects text in the file."""

        imageContext = key
        client = vision.ImageAnnotatorClient()

        with io.open(captcha_img_path, 'rb') as image_file:
            content = image_file.read()

        image = vision.Image(content=content)

        response = client.text_detection(
            image=image,
            image_context={"language_hints": ["bn"]},  # Bengali
        )
        # response = client.text_detection(image=image)
        texts = response.text_annotations
        captcha_info = []
        for text in texts:

            captcha_info.append('\n"{}"'.format(text.description).replace('\n', '#'))
            vertices = (['({},{})'.format(vertex.x, vertex.y)
                         for vertex in text.bounding_poly.vertices])

        return captcha_info[0].split("#")


if __name__ == '__main__':
    captcha_text = CaptchaText()
    captcha_img = "F:/CaptchaOCR/CaptchaImages/4.png"
    text_from_captcha = captcha_text.detect_text(captcha_img)
    text_from_captcha.remove('')
    text_from_captcha.remove('"')
    # print(text_from_captcha)
    for item in text_from_captcha:
        item = ''.join(item.split())
        item = item.replace('I', '1')
        item = item.strip('"')
        print(item)
